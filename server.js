const express = require('express')
const bodyParser = require('body-parser')


const routerUser = require('./routes/user')
const routerAdmin = require('./routes/admin')

const app = express()


app.use(bodyParser.json())




app.use('/user', routerUser)
app.use('/admin', routerAdmin)

app.listen(3537, '0.0.0.0', () => {
  console.log('server started on port 3537')
})
